import numpy as np
from sklearn import preprocessing
from sompy import SOMFactory
from utils import SOMClassifier, read, show_confusion_matrix

#Dimension de la red
dims = [3, 6, 10, 20, 30, 50]

#Obtencion de las bases de datos y los nombres de las caracteristicas
fnames, datatrain, labelstrain, datavalidation, labelsvalidation = read()

results_file  = open('accs.txt', 'w')

for i in dims:
    print('RAW '+str(i)+'x'+str(i))
    
    #Creacion de la red
    sm = SOMFactory().build(datatrain, mapsize=[i, i], normalization = False, initialization='pca', component_names=fnames)
    
    #Entrenamiento
    sm.train(verbose=False)
    
    #Error topografico y de cuantizacion
    topographic_error = sm.calculate_topographic_error()
    quantization_error = np.mean(sm._bmu[1])
    
    somcls = SOMClassifier()
    somcls.auto_label(sm, datatrain, labelstrain)
    
    conf_matrix, acc = somcls.classify(sm, datavalidation, labelsvalidation)
    
    results_file.write(('Mapa'+str(i)+'x'+str(i)+' RAW acc: '+str(acc))+'\n')
    results_file.write(('Mapa'+str(i)+'x'+str(i)+' RAW top_error: '+str(topographic_error)+'\n'))
    results_file.write(('Mapa'+str(i)+'x'+str(i)+' RAW quan_error: '+str(quantization_error)+'\n\n'))    
    
    show_confusion_matrix(conf_matrix, 'imgs/raw_'+str(i))

#Mismo proceso pero con datos normalizados
scaler = preprocessing.StandardScaler().fit(datatrain)
datatrain = scaler.transform(datatrain)
datavalidation = scaler.transform(datavalidation)

for i in dims:
    print('NORM '+str(i)+'x'+str(i))
    
    #Creacion de la red
    sm = SOMFactory().build(datatrain, mapsize=[i, i], normalization = 'var', initialization='pca', component_names=fnames)
    
    #Entrenamiento
    sm.train(verbose=False)
    
    #Error topografico y de cuantizacion
    topographic_error = sm.calculate_topographic_error()
    quantization_error = np.mean(sm._bmu[1])
    
    somcls = SOMClassifier()
    somcls.auto_label(sm, datatrain, labelstrain)
    
    conf_matrix, acc = somcls.classify(sm, datavalidation, labelsvalidation)

    results_file.write(('Mapa'+str(i)+'x'+str(i)+' NORM acc: '+str(acc)+'\n'))
    results_file.write(('Mapa'+str(i)+'x'+str(i)+' NORM top_error: '+str(topographic_error)+'\n'))
    results_file.write(('Mapa'+str(i)+'x'+str(i)+' NORM quan_error: '+str(quantization_error)+'\n\n')) 
    
    show_confusion_matrix(conf_matrix, 'imgs/norm_'+str(i))

results_file.close()