import unicodedata
import scipy.io as sio
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split

class SOMClassifier(object):
  _som = {}
  _votes = [];
  _bestlab = [];
   
  def auto_label(self, som, data, labels):
    _som = som;
    w = som.codebook.mapsize[0];
    h = som.codebook.mapsize[1];
    self._votes = np.zeros((w,h,4))
    bmu = som.find_bmu(data)[0];
    for i in range( data.shape[0] ):
      x = int(bmu[i] / w)
      y = int(bmu[i]) - w*x;
      l = int(labels[i]);
      self._votes[x,y,l] = self._votes[x,y,l] + 1;
    #self._bestlab = np.zeros((w,h))
    self._bestlab = np.zeros(w*h)
    for x in range(w):
        for y in range(h):
          best = np.argmax(  np.ravel(self._votes[x,y,:])  );
          if (best == 0):
            best = 2;
          self._bestlab[x*w+y] = best
         
  def classify(self, sm, data, labels):
    mybmu = sm.find_bmu(data)[0];
    mybmu = [int(x) for x in mybmu]
    labelspred = self._bestlab[mybmu]
    
    labels = [int(x) for x in labels]
    labels = np.array(labels)
    
    y_val = np.ravel(labels)
    y_pred = np.ravel(labelspred)
    
    conf = confusion_matrix(np.ravel(labels), np.ravel(labelspred))
    acc = accuracy_score(y_val, y_pred)

    return conf, acc


#Para leer datos vinos
def read():
    mat_contents = sio.loadmat('Base_de_datos_Tarea5.mat')
    
    data = mat_contents['muestras']
    fnames_0 = list(mat_contents['headers'][0])
    labels_0 = list(mat_contents['labels'])
    
    
    fnames = [];
    for fname in fnames_0:
      fnames.append(unicodedata.normalize('NFKD', fname[0])) #.encode('ascii','ignore'))
    
    cnames = [];
    for i in range(data.shape[0]):
      cnames.append("") #.encode('ascii','ignore'))
    
    labels = [];
    for label in labels_0:
      labels.append(unicodedata.normalize('NFKD', label[0][0])) #.encode('ascii','ignore'))
    
    #Partimos la base entre entrenamiento y test
    datatrain, datatest, labelstrain, labelstest, cnamestrain, cnamestest = train_test_split(data,labels,cnames, test_size=0.2);
        
    #Volvemos a partir la base de datos de entrenamiento
    datatrain, datavalidation, labelstrain, labelsvalidation, cnamestrain, cnamesvalidation = train_test_split(datatrain,labelstrain,cnamestrain, test_size=0.25);

    return fnames, datatrain, labelstrain, datavalidation, labelsvalidation, datatest, labelstest

def show_confusion_matrix(m, file_name):
    fig, ax = plt.subplots()    
    ax.matshow(m, cmap=plt.cm.Wistia)
    
    for i in range(len(m)):
        for j in range(len(m)):
            c = m[j,i]
            ax.text(i, j, str(c), va='center', ha='center')
            
    plt.savefig(file_name+'.png', format='png', dpi=1000)