import numpy as np
from sklearn import preprocessing
from sompy import SOMFactory
from utils import SOMClassifier, read, show_confusion_matrix
from sompy.visualization.mapview import View2D

#Dimension de la red
dim = 6

#Obtencion de las bases de datos y los nombres de las caracteristicas
fnames, datatrain, labelstrain, datavalidation, labelsvalidation, datatest, labelstest = read()

#Mismo proceso pero con datos normalizados
scaler = preprocessing.StandardScaler().fit(datatrain)
datatrain = scaler.transform(datatrain)
datatest = scaler.transform(datatest)

#Creacion de la red
sm = SOMFactory().build(datatrain, mapsize=[dim, dim], normalization = 'var', initialization='pca', component_names=fnames)

#Entrenamiento
sm.train(verbose=False)

#Error topografico y de cuantizacion
topographic_error = sm.calculate_topographic_error()
quantization_error = np.mean(sm._bmu[1])

somcls = SOMClassifier()
somcls.auto_label(sm, datatrain, labelstrain)

conf_matrix, acc = somcls.classify(sm, datatest, labelstest)

show_confusion_matrix(conf_matrix, 'imgs/final')

view2D = View2D(dim, dim, "rand data", text_size=10)
view2D.show(sm, col_sz=4, which_dim="all", desnormalize=True)